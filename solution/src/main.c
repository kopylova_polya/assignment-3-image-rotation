//Polina Kopylova made it

#include "../include/bmp/read.h"
#include "../include/bmp/transform-rotate.h"
#include "../include/bmp/write.h"
#include "../include/file/close.h"
#include "../include/file/open.h"
#include "../include/util.h"
#include <stdio.h>
#include <stdlib.h>


int main( int argc, char** argv ) {

    if(argc>3) {
        print_error("There is more than 3(needed) arguments");
        return 0;}
    if(argc<3){
        print_error("There is less than 3(needed) arguments");
        return 0;
    }

    print_correct("The number of arguments is correct");

    if(!argv[1]|| !argv[2]){
        print_error("One or both files doesnt exist. Check out them");
        return 0;
    }
    print_correct("Both files exist");

    const char* in_name = argv[1];
    const char* out_name = argv[2];
    FILE *in, *out;

    if( open_file(&in, in_name, READ)==ERROR_OPEN){
        print_error("Cant open the file for reading");
        return 0;
    }

    if( open_file(&out, out_name, WRITE)==ERROR_OPEN){
        print_error("Cant open the file for writing");
        return 0;
    }

    print_correct("Both files are able to be opened");

    struct image* image = malloc(IMAGE_STRUCT_SIZE);
    if(from_bmp(in, image)!=OK){
        print_error("Cant read test-image. Closing files...");

        close_file(in);
        close_file(out);
        free(image -> data);
        return 0;

    }
    print_correct("Reading test-image...");

    struct image transformed_image = rotate(*image);
    print_correct("Image was transformed! Clearing memory...");

    free(image->data);
    free(image);

    to_bmp(out, &transformed_image);
    free(transformed_image.data);
    print_correct("Closing files...");

    fclose(in);
    fclose(out);
    print_correct("Work is done now ^^");

    return 0;    
    
}

