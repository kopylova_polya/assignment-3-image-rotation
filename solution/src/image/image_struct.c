#include "../include/image/image_struct.h"
#include <stdio.h>
#include <stdlib.h>
struct image struct_create_image_with_width_height(const uint64_t w, const uint64_t h){
    struct image img = {0};

    img.width = w;
    img.height = h;

    struct pixel* data = malloc(w * h * sizeof(struct pixel));

    img.data = data;
    return img;
}

void free_image(struct image* image){
    free(image->data);
}
