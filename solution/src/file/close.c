//Polina Kopylova made it

 
#include "../../include/file/close.h"

enum close_status close_file(FILE* file) {
    //если не существует файла
    if (file==NULL) {
        return ERROR_CLOSE;
    }
    //при успешном закрытии возращает 0
    if (fclose(file)!=0) {
        return ERROR_CLOSE;
    }
    return SUCCESS_CLOSE;
}
