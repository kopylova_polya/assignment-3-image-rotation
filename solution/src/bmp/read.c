//Polina Kopylova made it

#include "../include/bmp/read.h"
#include "../include/bmp/headers.h"
#include "../include/image/image_struct.h"
#include <stdio.h>
#include <stdlib.h>

#define NUM_OF_READING_STRUCTS 1
#define NUM_OF_PIXEL_ARGS 3


size_t get_padding(size_t width){
    return (4 - (sizeof(struct pixel) * width)) % 4;
}

enum read_status from_bmp(FILE* const in, struct image* img){
    struct bmp_header hd = {0};

    if (fread(&hd, sizeof(struct bmp_header), 1, in) != 1){
        return ERROR_HEADER;
    }

    *img = struct_create_image_with_width_height(hd.biWidth, hd.biHeight);

    const size_t pad= get_padding(hd.biWidth);

    for (size_t i = 0; i < img->height; ++i){
        if (fread(img->data + (i * img->width), sizeof(struct pixel), img->width, in) != img->width) {
            free_image(img);
            return INVALID_SIGNATURE;
           
        }

        if (fseek(in, (long) pad, SEEK_CUR) != 0) {
            free_image(img);
            return ERROR_BITS;
        }
    }
    return OK;
}
