//Polina Kopylova made it

#include "../include/bmp/transform-rotate.h"
#include <stdlib.h>


void calculate_pixel_address(const uint64_t h,const uint64_t w,const struct image source,struct image rotated){
    for (uint64_t n = 0; n < h;  n++) {
        for (uint64_t m = 0; m < w; m++) {
            rotated.data[m * h + h - n - 1] = source.data[n * w + m];
        }
    }
}

struct image rotate( const struct image source ) {
    const uint64_t h = source.height;
    
    const uint64_t w = source.width;

    struct image rotated;
    rotated.height = w;
    rotated.width = h;
    rotated.data = malloc(PIXEL_STRUCT_SIZE * w * h);

    calculate_pixel_address(h,w,source,rotated);

    return rotated;
}

