//Polina Kopylova made it

#ifndef HEADERS
#define HEADERS

#define BMP_HEADER_STRUCT_SIZE sizeof(struct bmp_header)
#define PIXEL_OFFSET 4
#define TYPE_OF_FILES 19778
// #define NUM_OF_BITS 24
#define RESERVED 0
// #define PIXEL_SIZE 4
#define BIT_COUNT 24
#define COMPRESSION 0
#define ALL_SIZE 54
#define PLANES 1
#define X_PIXELS 2834
#define Y_PIXELS 2834
#define INFO_SIZE 40

#include  <stdint.h>

struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));

struct bmp_header* create_header(uint32_t file_size,uint32_t width,uint32_t height,uint32_t size_image);

#endif


