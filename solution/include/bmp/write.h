//Polina Kopylova made it

#include "../image/image_struct.h"
#include <stdio.h>

#ifndef WRITER
#define WRITER

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR = 1

};

enum write_status to_bmp( FILE* out, struct image const* img );

#endif 
