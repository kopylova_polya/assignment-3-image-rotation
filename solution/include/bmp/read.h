//Polina Kopylova made it
#include "../image/image_struct.h"
#include "../image/image_struct.h"
#include <stdio.h>

#ifndef READER
#define READER

/*  deserializer   */
enum read_status  {
    OK = 0,
    ERROR_BITS = 1,
    ERROR_HEADER = 2,
    INVALID_SIGNATURE = 3
};

enum read_status from_bmp( FILE* in, struct image* img );

#endif
