//Polina Kopylova made it

#ifndef IMG_STRUCT
#define IMG_STRUCT

#include <inttypes.h>

#define PIXEL_STRUCT_SIZE sizeof(struct pixel)
#define IMAGE_STRUCT_SIZE sizeof(struct image)

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image struct_create_image_with_width_height(const uint64_t width, const uint64_t height);
void free_image(struct image* image);

#endif
